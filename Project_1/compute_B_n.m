% compute B^n to use full numeric range in computing E-step
function [B_n] = compute_B_n(E,y)
% H: number of bars
% theta:
    % pi: probability of a bar to be on
    % sigma: broadness of gaussian noise
    % W: weights, here: mapping from bars to vector
% y: data points

A_max = log(realmax(class(y)));      % max value for which exp(A_max) is not inf

% compute minimal possible value of energy
E_min = min(min(E));

B_n = A_max+E_min-10;  % make sure B^n-E is always smaller than A_max -> exp(B^n-E) <inf
end
