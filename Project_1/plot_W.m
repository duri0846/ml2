function [] = plot_W(W,p)
% plots weights W in rows of 10
% W: weights, here: mapping from bars to vector

n = size(W,2);      % number of data points
[r,c]=numSubplots(n);
% figure
for i = 1:n         
    Wn = reshape(W(:,i),p,p);   
    subplot(r(1),r(2),i)            
    imshow(Wn,[0,max(max(W))], 'InitialMagnification', 10000)     % show images in gray values (values between 0 and 1)    
    drawnow;
end
                                           % put title only on first image without noise
        sgtitle('Learned weights')
    
end