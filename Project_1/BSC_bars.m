clear
close all
% main script
% this script generates data with gaussian noise for a BSC-bars-plot according to the parameters of theta_gen (pi, sigma and W)
% afterwards it tries to learn the values of theta by an EM-algorithm

tic

% image parameters
p = 3;   % number of pixels horizontolly and vertically
P = p^2; % number of pixels in whole image/ D
H = 2*p; % number of bars

% number of data points
num_data_points = 1000;   

benchmark_criterion = 1; % 1 == log-likelihood, 0 == pi

% generate data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Theta (for generation) as a struct
theta_gen.pi = 2/H;         % probability of a bar to be on
theta_gen.sigma_squared = 0.2; % width of gaussian noise
theta_gen.W = zeros(P,H);   % weights/ mapping from s to bars
% compute W in two for-loops
for i = 1:p                 % vertical bars
    theta_gen.W((i-1)*p+1:(i-1)*p+p,i) = 1;
end
for i = 1:p                 % horizontal bars
    theta_gen.W(i:p:P, p+i) = 1;
end

% generate data
[y, s] = generate_BSC(p,theta_gen,num_data_points);

plot_data1(p,theta_gen.W,y,s,20);

% counter and values for breaking EM-loop
counter = 0;
counter_max = 4;
it_counter = 0;
log_likelihood = -Inf;
diff_min = 1e-2;


% EM-Algorithm ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% initialize theta_old
theta_old.pi = 0.1;
pi_record = theta_old.pi;
theta_old.sigma_squared = 1;
sigma_squared_record = theta_old.sigma_squared;
theta_old.W = rand(P,H);
s_poss = ff2n(H)';   % full factorial design matrix for all possible s


while true
    
it_counter = it_counter + 1;

% E-step ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[s_exp, ss_exp] = compute_E_step(y, theta_old,s_poss);

% M-step ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% compute theta_new (pi, sigma, W)
theta_new.pi = 0;
theta_new.sigma_squared = 0;
theta_new.W = zeros(P,H);

% compute W first, because it's value is needed for the computation of
% sigma_squared
theta_new.W = y*s_exp'/(sum(ss_exp,3));

% compute pi and sigma_squared
for i = 1:num_data_points
    theta_new.pi = theta_new.pi + sqrt(s_exp(:,i))'*sqrt(s_exp(:,i));
    theta_new.sigma_squared = theta_new.sigma_squared + y(:,i)'*y(:,i)+trace(theta_new.W'*theta_new.W*ss_exp(:,:,i))-2*y(:,i)'*theta_new.W*s_exp(:,i);
end

theta_new.pi = theta_new.pi/(num_data_points*H);
theta_new.sigma_squared = theta_new.sigma_squared/(num_data_points*P);

pi_record(it_counter+1) = theta_new.pi;
sigma_squared_record(it_counter+1) = theta_new.sigma_squared;

if benchmark_criterion == 1
    temp = log_likelihood;
    % compute log-likelihood
    log_likelihood = 0;
    for c1 = 1: num_data_points
        for c2 = 1:size(s_poss,2)
            log_likelihood = log_likelihood - 1/2*log(det(2*pi*theta_new.sigma_squared*eye(P)))...
                -1/(2*theta_new.sigma_squared)*(y(:,c1)-theta_new.W*s_poss(:,c2))'*(y(:,c1)-theta_new.W*s_poss(:,c2))...      
            + sum(s_poss(:,c2)*log(theta_new.pi)+(1-s_poss(:,c2))*log(1-theta_new.pi));
        end
    end

    log_likelihood = log_likelihood/(num_data_points*2^H);

    log_diff = temp-log_likelihood;

    % adjust counter
    if abs(log_diff) < diff_min
        counter = counter + 1;
    else
        counter = 0;
    end

    % plot loglikelihood
    figure(11)
    pos1 = get(gcf,'Position');
    psize = get(gcf,'PaperSize');
    hold on
    plot(it_counter,-log_likelihood,'ko','MarkerSize',3);
    xlabel('iteration')
    ylabel('log-likelihood per datapoint/ 2^H')
    drawnow;

    figure(12)
    pos2 = get(gcf,'Position');
    set(gcf,'Position', [pos1(1)-550,pos1(2),pos1(3),pos1(4)])
    plot_W(theta_new.W,p)
else
    pi_diff = theta_old.pi-theta_new.pi;

    % adjust counter
    if abs(pi_diff) < diff_min
        counter = counter + 1;
    else
        counter = 0;
    end
end
% set theta_old = theta_new
theta_old = theta_new;

if counter >= counter_max
    break
end

% stirr things up a little bit to get out of local extrema 
% instead of annealing sigma_squared = sigma_squared/beta with beta(it_counter)
for ind1 = 1:P
    for ind2 = 1:H
        if theta_old.W(ind1,ind2) < max(max(theta_old.W))*1e-3
          theta_old.W(ind1,ind2) = max(max(theta_old.W))*1e-3;
        end
    end
end

end

toc
if benchmark_criterion == 0
    figure(1)
    plot_W(theta_new.W,p)
    pos1 = get(gcf,'Position');
    psize = get(gcf,'PaperSize');
    hold on
end

figure(2)
hold on
set(gcf,'Position', [pos1(1)+550,pos1(2),pos1(3),pos1(4)])
plot(0:length(sigma_squared_record)-1, pi_record, 'go','MarkerSize',3);
xlabel('iteration')
ylabel('pi')

figure(3)
hold on
set(gcf,'Position', [pos1(1)+550,pos1(2)-450,pos1(3),pos1(4)])
plot(0:length(sigma_squared_record)-1, sigma_squared_record, 'ro','MarkerSize',3);
xlabel('iteration')
ylabel('sigma^2')


pi = theta_new.pi
sigma_squared = theta_new.sigma_squared