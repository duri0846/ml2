% this script generates BSC data points; calculates the log-likelihood;
% compares it with the likelihood of random noise

clear
close all
% main script
% this script generates data with gaussian noise for a BSC-bars-plot according to the parameters of theta_gen (pi, sigma and W)
% afterwards it tries to learn the valus of theta by an EM-algorithm

% image parameters
p = 2;   % number of pixels horizontolly and vertically
P = p^2; % number of pixels in whole image
H = 2*p; % number of bars

% number of data points
num_data_points = 50;   

% generate data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Theta (for generation) as a struct
theta_gen.pi = 0.2;         % probability of a bar to be on
theta_gen.sigma_squared = 0.01; % width of gaussian noise
theta_gen.W = zeros(P,H);   % weights/ mapping from s to bars
% compute W in two for-loops
for i = 1:p                 % vertical bars
    theta_gen.W((i-1)*p+(1:p),i) = 0.5*ones(1,p);
end
for i = 1:p                 % horizontal bars
    theta_gen.W(i:p:P, p+i) = 0.5;
end

% generate data
[y, s] = generate_BSC(p,theta_gen,num_data_points);
% % plot data
% plot_W(theta_gen.W,p);    % plot data with and without noise

% calculate likelihood for BSC data points
l = compute_loglike(y, theta_gen.pi, theta_gen.sigma_squared, theta_gen.W);
% l = 0;
% for i = 1:size(y,2)
%     
%     y_n = y(:,i);
%     l_n = compute_loglike(y_n, theta_gen.pi, theta_gen.sigma_squared, theta_gen.W);   
%     l = l + l_n;
%     
% end

% generate random noice and calculate likelihood
y_rand = rand(size(y));
l_rand = compute_loglike(y_rand, theta_gen.pi, theta_gen.sigma_squared, theta_gen.W);
% l_rand = 0;
% for i = 1:size(y_rand,2)
%     
%     y_n = y_rand(:,i);
%     l_n = compute_loglike(y_n, theta_gen.pi, theta_gen.sigma_squared, theta_gen.W);   
%     l_rand = l_rand + l_n;
%     
% end


barh(1,l)
hold on
barh(2,l_rand)
xlabel('log-likelihood')
ylabel('data')
legend({'generated BSC','random'})
hold off
