% compute E for all data points as needed for computation of <s> and <ss'>
% E: part of joint probability/posterior that depends on s  
function [E] = compute_E(y,theta,s)
% y: data points 
% theta:
    % pi: probability of a bar to be on
    % sigma: broadness of gaussian noise
    % W: weights, here: mapping from bars to vector
    
n = size(y,2);  % number of data points
m = size(s,2);  % number of possibilities of s
E = zeros(n,m);
for i = 1:n
    for j = 1:m
        E(i,j) = 1/(2*theta.sigma_squared)*(y(:,i)-theta.W*s(:,j))'*(y(:,i)-theta.W*s(:,j))...
            -log(theta.pi/(1-theta.pi))*norm(s(:,j));
    end
end
end
