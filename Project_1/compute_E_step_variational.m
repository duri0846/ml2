% computes variational E-step
% gives out expectation values of s and ss'
function [s_exp, ss_exp, lambda_new] = compute_E_step_variational(y, theta_old, lambda_old)

count_limit = 15;       % limit of variational loop count

H = 2*sqrt(size(y,1));  % number of possible bars
n = size(y,2);          % number of data points

% initialize <s> and <ss'>
s_exp = zeros(H,n);
ss_exp = zeros(H,H,n);

lambda_temp = lambda_old;

for iter = 1:n
    
    s_n_record = [];
    
    y_n = y(:,iter);                                        % current data point
    s_old = lambda_old(:,iter);                             % initial s for variational loop
    E_old = compute_E_variational(y_n,theta_old,s_old);     % initial E for variational loop
    
    count_loop = 0;                                       % record the variational loop count
    Marker = false;                                         % whether goes into variational loop better s was found
    
    while true && size(s_n_record,1)<=2^H                   % variational loop        
       
        count_loop = count_loop+1;
        
        if count_loop == count_limit                      % limit the variational loop
            break
        end
        
        s = double(rand(H, 1)< theta_old.pi);               % draw a s from prior distribution(Bernuli)

        if ~isempty(s_n_record) && ~ismember(s', s_n_record, 'rows')                 % if this s value has not apeared yet
           
            E = compute_E_variational(y_n,theta_old,s);      % calculate E based on the s

            if E < E_old                                     % E is smaller means log-joint is larger
                lambda_temp(:, iter)    =  s;                   % update lambda
                Marker                  = not(Marker);
                break;                                          % end variational loop and go to next data
            end                                                 % teminate at most 2^H times
            
            
            s_n_record(end+1,:) = s';                           % record s
            
        elseif isempty(s_n_record)
            s_n_record = s';
        
        else
            continue
        end
    end
    if Marker == true
        s_exp(:,iter) = s;
        ss_exp(:,:, iter) = s*s';
    else
        s_exp(:,iter) = s_old;
        ss_exp(:,:, iter) = s_old*s_old';
    end
    
end

lambda_new = lambda_temp;


end
