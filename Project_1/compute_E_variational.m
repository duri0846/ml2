% compute E for a single data  point and a sigle s as needed for
% variational loop
% E: part of log-joint probability/posterior that depends on s (the sign of E is opposite as log-joint)
function [E] = compute_E_variational(y,theta,s)
% y: one data point
% theta:
% pi: probability of a bar to be on
% sigma: broadness of gaussian noise
% W: weights, here: mapping from bars to vector
% s: one vector


E = 1/(2*theta.sigma_squared)*(y-theta.W*s)'*(y-theta.W*s)...
     -log(theta.pi/(1-theta.pi))*norm(s); 
end
