% plots data in rows of 10 with and without gaussian noise
function plot_data1(p,W,y,s,n)
% p: pixels horizontally/vertically
% W: weights, here: mapping from bars to vector
% y: data points 
% s: binary latent

if nargin < 5
    n = size(y,2);      % number of data points
end
D = 10;             % images per row    
m = ceil(n/D);      % number of columns
rest = D-mod(n,D);  % blank space after last image
if rest == D
    rest = 0;
end
figure(5)
for i = 1:n         % for all data points
    yn = reshape(y(:,i),p,p);   % with noise - reshape to quadratic matrix for display
    y0 = reshape(W*s(:,i),p,p); % without noise
    subplot(2*m,D,i)            % first m x D - with noise
    imshow(yn,[0,1], 'InitialMagnification', 10000)     % show images in gray values (values between 0 and 1)
    if i==1                                             % put title only on first image with noise
        title('Generated BSC Data')
    end
    subplot(2*m,D,i+n+rest)     % second m x D - without noise
    imshow(y0,[0,max(max(W))], 'InitialMagnification', 10000)
    if i == 1                                           % put title only on first image without noise
        title('Without Gaussian Noise')
    end
end
end
