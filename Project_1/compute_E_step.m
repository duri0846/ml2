% computes E-step
% gives out expectation values of s and ss'
function [s_exp, ss_exp] = compute_E_step(y, theta_old,s)

H = 2*sqrt(size(y,1));  % number of possible bars
n = size(y,2);  % number of data points

%compute E
E = compute_E(y,theta_old,s);

% compute B_n
B_n = compute_B_n(E,y);

% initialize <s> and <ss'>
s_exp = zeros(H,n);
ss_exp = zeros(H,H,n);

for iter = 1:n
    s_exp_num = zeros(H,1); % numerator
    s_exp_den = zeros(H,1); % denominator

    ss_exp_num = zeros(H,H);% numerator
    ss_exp_den = zeros(H,H);% denominator

        for i = 1:size(s,2) % for all possibilities of s (2^H)
            % compute expectation value of s unsing exp of log-pseudo-joint: exp(B_n-E)
            % internal energy of the generative model: E
            s_exp_num = s_exp_num + exp(B_n-E(iter,i))*s(:,i);
            s_exp_den = s_exp_den + exp(B_n-E(iter,i));
            % compute expectation value of ss'
            ss_exp_num = ss_exp_num + exp(B_n-E(iter,i))*s(:,i)*s(:,i)';
            ss_exp_den = ss_exp_den + exp(B_n-E(iter,i));
        end
        
    epsilon = 1e-50;   
    
    s_exp(:,iter)  = (s_exp_num)./(s_exp_den +epsilon);
    ss_exp(:,:,iter) = (ss_exp_num)./(ss_exp_den +epsilon);
end
end
