% generate BSC-data for bars in quadratic image with gaussian noise
function [y, s] = generate_BSC(p,theta,n)
    % p: pixels horizontally/vertically
    % theta:
        % pi: probability of a bar to be on
        % sigma: broadness of gaussian noise
        % W: weights, here: mapping from bars to vector
    
    % generate causes (switching bars on/off)
    s = zeros(2*p,n);
    s(rand(size(s))<=theta.pi)=1;  % random number between 0 and 1 from a uniform distribution
    % if random number is smaller or equal to theta.pi, the bar is on/ s-value is 1

    % weigth s (map s to vector) and add noise
    y = theta.W*s + randn(p^2,n)*sqrt(theta.sigma_squared);
      
end
