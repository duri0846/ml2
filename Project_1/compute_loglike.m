 function [l] = compute_loglike(y_n, pi_p, sigma2, W)

% y_n = y;
% pi_p = theta_gen.pi;         % probability of a bar to be on
% sigma2 = theta_gen.sigma_squared; % width of gaussian noise
% W = theta_gen.W;
    % this function calculate the log-likelihood of a genetated data set of BSC
    
    % the output is the value of log-likelihood
    
    % y_n is a single collumn data point
    % pi_p is prior probability
    % sigma2 is square of sigma value
    % W is bar pattern matrix, each collumn represents a pattern with one bar in the image
    l = 0;
    for i = 1:size(y_n, 2)
        y_row = y_n(:, i)';                           % change to row vector
        s = ff2n(2*sqrt(size(W, 1)))';          % full factorial design matrix for all possible s    
        p_n = 0;                                % initialize p_n, likelihood for each s

        for j = 1:size(s,2)                     % for all possible s

            s_j = s(:,j);                       % current s 
            %p_s = (pi_p^(sum(s_j)))*((1-pi_p)^(sum(not(s_j)))); % prosterior
            p_s = log(pi_p/(1-pi_p))*norm(s_j,1) + length(s_j)*log(1-pi_p);
           %p_y = mvnpdf(y_row,zeros(size(y_row)),sqrt(sigma2)*eye(length(y_row)));
           p_y = -0.5*log(det(2*pi*sigma2*eye(length(y_row))))-1/(2*sigma2)*(y_row'-W*s_j)'*(y_row'-W*s_j);

            p_n = p_n + p_y*p_s;                % sum up the likelihood for all s
        end
           l = l +  p_n;                 % log-likelihood for sigle data y_n
            
    end
    
 end
