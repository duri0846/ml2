clear
% close all
% main script
% this script generates data with gaussian noise for a BSC-bars-plot according to the parameters of theta_gen (pi, sigma and W)
% afterwards it tries to learn the values of theta by a truncated variational EM-algorithm



% image parameters
p = 3;   % number of pixels horizontolly and vertically
P = p^2; % number of pixels in whole image
H = 2*p; % number of bars

% number of data points
num_data_points = 500;   

% generate data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Theta (for generation) as a struct
theta_gen.pi = 1/3;         % probability of a bar to be on
theta_gen.sigma_squared = 2; % width of gaussian noise
theta_gen.W = zeros(P,H);   % weights/ mapping from s to bars
% compute W in two for-loops
for i = 1:p                 % vertical bars
    theta_gen.W((i-1)*p+(1:p),i) = 10*ones(1,p);
end
for i = 1:p                 % horizontal bars
    theta_gen.W(i:p:P, p+i) = 10;
end

% generate data
[y, s] = generate_BSC(p,theta_gen,num_data_points);

% plot_data1(p,theta_gen.W,y,s,30);

% counter and values for breaking EM-loop
counter = 0;
counter_max = 10;
it_counter = 0;
continue_ = true;
diff_min = 1e-3;

% EM-Algorithm ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% initialize theta_old
theta_old.pi = 0.1;
pi_record = theta_old.pi;
theta_old.sigma_squared = 1;
sigma_squared_record = theta_old.sigma_squared;
theta_old.W = rand(P,H);
lambda_old = double((rand(H,num_data_points)<=theta_old.pi)); % matrix of s, form (H, N), initialize lemada drawn from Bernuli distribution

tic

while continue_
    
it_counter = it_counter + 1;

% E-step ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[s_exp, ss_exp, lambda_new] = compute_E_step_variational(y, theta_old,lambda_old);

% M-step ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% compute theta_new (pi, sigma, W)
theta_new.pi = sum(sum(s_exp,2))/(num_data_points*H);                     % pi_new = sum(<|s|>_q)/NH
theta_new.W = y*s_exp'/(sum(ss_exp,3));

theta_new.sigma_squared = 0;
for i = 1:num_data_points
    theta_new.sigma_squared = theta_new.sigma_squared + y(:,i)'*y(:,i)+trace(theta_new.W'*theta_new.W*ss_exp(:,:,i))-2*y(:,i)'*theta_new.W*s_exp(:,i);
end
theta_new.sigma_squared  = theta_new.sigma_squared /(num_data_points*P);

pi_diff = theta_old.pi-theta_new.pi;
pi_record(it_counter+1) = theta_new.pi;
sigma_squared_record(it_counter+1) = theta_new.sigma_squared;

% set theta_old = theta_new
theta_old = theta_new;
lambda_old = lambda_new;

% adjust counter
if abs(pi_diff) < diff_min
    counter = counter + 1;
else
    counter = 0;
end

if counter >= counter_max
    continue_ = false;
end


% stirr things up a little bit to get out of local extrema
for ind1 = 1:P
    for ind2 = 1:H
        if theta_old.W(ind1,ind2) < max(max(theta_old.W))*1e-3
          theta_old.W(ind1,ind2) = rand*max(max(theta_old.W))*1e-3;
        end
    end
end
end

t = toc;

figure(1)
plot_W(theta_new.W,p)
pos1 = get(gcf,'Position');
psize = get(gcf,'PaperSize');
hold on

figure(2)
hold on
set(gcf,'Position', [pos1(1)+550,pos1(2),pos1(3),pos1(4)])
plot(0:length(sigma_squared_record)-1, pi_record, 'go','MarkerSize',3);
xlabel('iteration')
ylabel('pi')

figure(3)
hold on
set(gcf,'Position', [pos1(1)+550,pos1(2)-450,pos1(3),pos1(4)])
plot(0:length(sigma_squared_record)-1, sigma_squared_record, 'ro','MarkerSize',3);
xlabel('iteration')
ylabel('sigma^2')

t
pi = theta_new.pi
sigma_squared = theta_new.sigma_squared
