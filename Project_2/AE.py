# Autoencoder

# Vanilla Autoencoder for data set of MNIST
# Structure of encoder -- decoder: 784-20-5-20-784


import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
import matplotlib.pyplot as plt

# parameter setting
num_epochs = 5
batch_size = 2 ** 6
lr = 5e-3  # learning rate

ImSize = [28, 28]  # size of images
LayerInterface = ImSize[0] * ImSize[1]  # number of input(output) layer notes
Layer2Notes = 20  # number of (last) second layer nodes
LayerMidNodes = 5  # number of latent layer nodes

# load data set
transform = transforms.ToTensor()  # scale to float tensor in the range of [0.0  1.0]

mnist_train = datasets.MNIST(root='./data', train=True, download=True, transform=transform)  # load training dataset
indices_train = [i for i in range(len(mnist_train)) if
           (mnist_train.targets[i] == 0 or mnist_train.targets[i] == 1)]  # index of only images of 0 and 1
mnist_train.data, mnist_train.targets = mnist_train.data[indices_train], mnist_train.targets[indices_train]  # only 0 and 1 images
train_loader = torch.utils.data.DataLoader(dataset=mnist_train, batch_size=batch_size,
                                           shuffle=False)  # dataset to batches

mnist_test = datasets.MNIST(root='./data', train=False, download=True, transform=transform)  # load testing dataset
indices_test = [i for i in range(len(mnist_test)) if
           (mnist_test.targets[i] == 0 or mnist_test.targets[i] == 1)]  # index of only images of 0 and 1
mnist_test.data, mnist_test.targets = mnist_test.data[indices_test], mnist_test.targets[indices_test]  # only images of 0 and 1
test_loader = torch.utils.data.DataLoader(dataset=mnist_test, batch_size=len(mnist_test.data),
                                          shuffle=False)  # all test data


# network structure
class AE(nn.Module):
    def __init__(self):
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Linear(LayerInterface, Layer2Notes),
            nn.LeakyReLU(),  # alternative: ReLU()
            nn.Linear(Layer2Notes, LayerMidNodes),

        )

        self.decoder = nn.Sequential(
            nn.Linear(LayerMidNodes, Layer2Notes),
            nn.LeakyReLU(), # alternative: ReLU()
            nn.Linear(Layer2Notes, LayerInterface),
            nn.Sigmoid()
        )

    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded


def weight_init(m):    # initialize weight of layers
    if isinstance(m, nn.Linear):
        nn.init.xavier_uniform_(m.weight.data)
#  alternatives: normal_, kaiming_normal_, kaiming_, uniform_, xavier_uniform_, xavier_normal_, constant_, eye_


model = AE()
model.apply(weight_init)

criterion = nn.MSELoss()  # mean square error as loss function
optimizer = optim.Adam(model.parameters(), lr=lr)  # Adam as optimizer


# training and testing
outputs_train = []  # for saving original images and decoded images of training set
outputs_test = []  # for saving original images and decoded images of testing set
WeightLastLayer = []  # for saving the weight values of 20 -> 748
LossHistory_train = []  # for saving evolution of training loss
LossHistory_test = []  # for saving evolution of testing loss

# training and testing
for epoch in range(num_epochs):
    # training
    LossValue_train = 0  # initial epoch accumulated loss over number of batches
    for (img_train, _) in train_loader:  # for each batch
        img_train = img_train.reshape(-1, LayerInterface)  # original images within the batch
        optimizer.zero_grad()  # clear gradients of last batch
        recon_train = model(img_train)  # reconstructed images from the original images by the network
        loss_train = criterion(recon_train, img_train)  # mean loss of each image in this batch
        LossValue_train += loss_train.item()  # sum up the mean loss of each batch

        loss_train.backward()  # Computes the gradient of current tensor w.r.t.
        optimizer.step()  # update weights

    WeightLastLayer.append(model.decoder[2].weight.reshape([ImSize[0], ImSize[1],
                                                            Layer2Notes]))  # weights between last two layers in decoder
    LossHistory_train.append(LossValue_train / len(train_loader))
    outputs_train.append((epoch, img_train, recon_train))

    # testing
    LossValue_Test = 0
    for (img_test, _) in test_loader:
        img_test = img_test.reshape(-1, LayerInterface)
        recon_test = model(img_test)
        loss_test = criterion(recon_test, img_test)
        LossValue_Test += loss_test.item()

    LossHistory_test.append(LossValue_Test / len(test_loader))
    outputs_test.append((epoch, img_test, recon_test))

    print(f'Epoch:{epoch + 1}, Train Loss:{LossHistory_train[-1]:.6f}, Test Loss:{LossHistory_test[-1]:.6f}')


# visualization
fig1 = plt.figure(figsize=(9, 2))
FigNum = 0
if num_epochs > 5:
    FigRow = len(range(0, num_epochs, int(num_epochs / 3))) + 1
    index_imgPlot = list(range(0, num_epochs, int(num_epochs / 3))) + [num_epochs - 1]  # plot only 5 of all epochs
if num_epochs <= 5:
    FigRow = num_epochs
    index_imgPlot = range(num_epochs)  # plot all epochs

for k in index_imgPlot:
    plt.gray()
    imgs_train = outputs_train[k][1].detach().numpy()
    recon_train = outputs_train[k][2].detach().numpy()
    for i, item in enumerate(imgs_train):  # first row original images
        if i >= 9 or k > 0:
            break
        plt.subplot(FigRow + 1, 9, i + 1)
        item = item.reshape(-1, ImSize[0], ImSize[1])
        plt.axis('off')
        plt.imshow(item[0])

    for i, item in enumerate(recon_train):  # from second row on, generated images during training
        if i >= 9:
            break
        plt.subplot(FigRow + 1, 9, 9 + FigNum + 1)
        FigNum += 1
        item = item.reshape(-1, ImSize[0], ImSize[1])
        plt.axis('off')
        plt.imshow(item[0])

    plt.show()
fig1.suptitle('Evolution while Training ')

fig2 = plt.figure()
line_train, = plt.plot(LossHistory_train, label='Training')
line_test, = plt.plot(LossHistory_test, label='Testing')
plt.legend(handles=[line_train, line_test])
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Loss History')

fig3 = plt.figure()
for i in range(Layer2Notes):
    plt.subplot(5, 4, i + 1)
    plt.axis('off')
    plt.imshow(WeightLastLayer[num_epochs - 1][:, :, i].detach().numpy())
fig3.suptitle('Weight of Last Layer')


img_test = outputs_test[num_epochs - 1][1].detach().numpy()
recon_test = outputs_test[num_epochs - 1][2].detach().numpy()

fig4 = plt.figure()
for i, item in enumerate(img_test):  # first row original images
    if i >= 9:
        break
    plt.subplot(2, 9, i + 1)
    item = item.reshape(-1, ImSize[0], ImSize[1])
    plt.axis('off')
    plt.imshow(item[0])

for i, item in enumerate(recon_test):  # second row generated images
    if i >= 9:
        break
    plt.subplot(2, 9, 9 + i + 1)
    item = item.reshape(-1, ImSize[0], ImSize[1])
    plt.axis('off')
    plt.imshow(item[0])
fig4.suptitle('Testing Result')
