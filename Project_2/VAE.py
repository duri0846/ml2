# Variational Autoencoder

# Variational Autoencoder for data set of MNIST
# Structure of encoder -- decoder: described in task sheet


import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from torchvision import datasets, transforms
import matplotlib.pyplot as plt

# parameter setting
num_epochs = 10
M = 10  # number of samples
batch_size = 2 ** 6
lr = 1e-3  # learning rate

ImSize = [28, 28]  # size of images
LayerInterface = ImSize[0] * ImSize[1]  # number of input(output) layer notes
Layer2Notes = 32  # number of (last) second layer nodes
LayerMidNodes = 16  # number of latent layer nodes

# load data set
transform = transforms.ToTensor()  # scale to float tensor in the range of [0.0  1.0]

mnist_train = datasets.MNIST(root='./data', train=True, download=True, transform=transform)  # load training dataset
indices_train = [i for i in range(len(mnist_train)) if
                 (mnist_train.targets[i] == 2 or mnist_train.targets[i] == 5 or mnist_train.targets[i] == 8)]  # index of only images of 0 and 1
mnist_train.data, mnist_train.targets = mnist_train.data[indices_train], mnist_train.targets[indices_train]
train_loader = torch.utils.data.DataLoader(dataset=mnist_train, batch_size=batch_size,
                                           shuffle=False)  # dataset to batches

mnist_test = datasets.MNIST(root='./data', train=False, download=True, transform=transform)  # load testing dataset
indices_test = [i for i in range(len(mnist_test)) if
                (mnist_test.targets[i] == 2 or mnist_test.targets[i] == 5 or mnist_test.targets[i] == 8)]  # index of only images of 0 and 1
mnist_test.data, mnist_test.targets = mnist_test.data[indices_test], mnist_test.targets[indices_test]
test_loader = torch.utils.data.DataLoader(dataset=mnist_test, batch_size=len(mnist_test.data),
                                          shuffle=False)


# network structure
class VAE(nn.Module):
    def __init__(self):
        super().__init__()
        self.encoder_L1 = nn.Sequential(
            nn.Linear(LayerInterface, Layer2Notes),
            nn.LeakyReLU(),  # alternative: ReLU()
        )

        self.encoder_L2_mu = nn.Sequential(
            nn.Linear(Layer2Notes, LayerMidNodes),
            nn.Identity()  # nn.LeakyReLU()
        )

        self.encoder_L2_logvar = nn.Sequential(
            nn.Linear(Layer2Notes, LayerMidNodes),
            nn.Sigmoid()  # nn.Sigmoid()
        )

        self.decoder = nn.Sequential(
            nn.Linear(LayerMidNodes, Layer2Notes),
            nn.LeakyReLU(),  # alternatives(from better to worse): LeakyReLU(), ReLU(), Tanh(), Sigmoid(), Softmax()
            nn.Linear(Layer2Notes, LayerInterface),
            nn.Sigmoid()
        )

    def encoder(self, x):
        h1 = self.encoder_L1(x)
        return self.encoder_L2_mu(h1), self.encoder_L2_logvar(h1)

    def reparameterize(self, mu, logvar):
        samples = torch.zeros([M, len(mu), LayerMidNodes])
        for i in range(M):
            std = torch.exp(0.5*logvar)  # log variance to standard deviation
            eps = torch.randn_like(std)  # auxiliary noise variable drawn from standard normal distribution
            samples[i, :, :] = mu + eps*std
        return samples

    def forward(self, x):
        recon_x = torch.zeros([len(x), M,  LayerInterface])
        mu, logvar = self.encoder(x)
        samples = self.reparameterize(mu, logvar)
        for i in range(M):
            recon_x[:, i, :] = self.decoder(samples[i])
        return recon_x, mu, logvar


def weight_init(m):    # initialize weight of layers
    if isinstance(m, nn.Linear):
        nn.init.xavier_uniform_(m.weight.data)
#  alternatives: normal_, kaiming_normal_, kaiming_, uniform_, xavier_uniform_, xavier_normal_, constant_, eye_


def loss(recon_x, x, mu, logvar):

    # Reconstruction term
    log_p_x = torch.distributions.Normal(recon_x, torch.tensor(1)).log_prob(x.unsqueeze(1))

    reconstruction_error = log_p_x.mean(dim=1).sum(dim=1)  # size is ( batch_size ,)
    # KL term
    std = torch.sqrt(torch.exp(logvar))
    q = torch.distributions.Normal(mu, std)  # variational posterior
    p_z = torch.distributions.Normal(torch.zeros_like(mu), torch.ones_like(std))  # 0-1 distribution

    KL = torch.distributions.kl_divergence(q, p_z).sum(dim=1)  # the size is ( batch_size ,)
    # negative averaged ELBO per datapoint
    return -(reconstruction_error - KL).mean()


def loss2(recon_x, x, mu, logvar):  # alternative loss function

    # Reconstruction term
    reconstruction_error = 0
    for i in range(M):
        reconstruction_error += (F.binary_cross_entropy(recon_x[:, i, :], x.view(-1, LayerInterface), reduction='sum'))/M

    # KL term
    KL = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return (reconstruction_error + KL)/len(recon_x)


model = VAE()
model.apply(weight_init)

optimizer = optim.Adam(model.parameters(), lr=lr)  # Adam as optimizer


# training and testing
outputs_train = []  # fo r saving original images and decoded images of training set
outputs_test = []  # for saving original images and decoded images of testing set
WeightLastLayer = []  # for saving the weight values of 32 -> 748
LossHistory_train = []  # for saving evolution of training loss
LossHistory_test = []  # for saving evolution of testing loss

for epoch in range(num_epochs):
    # training
    LossValue_train = 0  # initial epoch accumulated loss over number of batches
    for (img_train, _) in train_loader:  # for each mini batch
        img_train = img_train.reshape(-1, LayerInterface)  # original images within the batch
        optimizer.zero_grad()
        recon_train_samples, mu, logvar = model(img_train)
        loss_train = loss2(recon_train_samples, img_train, mu, logvar)  # mean loss of each image in this batch
        LossValue_train += loss_train.item()  # sum up the mean loss of each batch

        loss_train.backward()
        optimizer.step()

    WeightLastLayer.append(model.decoder[2].weight.reshape([ImSize[0], ImSize[1],
                                                            Layer2Notes]))  # weights between last two layers in decoder
    LossHistory_train.append(LossValue_train / len(train_loader))
    outputs_train.append((epoch, img_train, recon_train_samples.mean(dim=1)))

    # testing
    LossValue_Test = 0
    for (img_test, _) in test_loader:
        img_test = img_test.reshape(-1, LayerInterface)
        recon_test_samples, mu, logvar = model(img_test)
        loss_test = loss2(recon_test_samples, img_test, mu, logvar)
        LossValue_Test += loss_test.item()

    LossHistory_test.append(LossValue_Test / len(test_loader))
    outputs_test.append((epoch, img_test, recon_test_samples.mean(dim=1)))

    print(f'Epoch:{epoch + 1}, Train Loss:{LossHistory_train[-1]:.6f}, Test Loss:{LossHistory_test[-1]:.6f}')


# visualization
fig1 = plt.figure(figsize=(9, 2))
FigNum = 0
if num_epochs > 5:
    FigRow = len(range(0, num_epochs, int(num_epochs / 3))) + 1
    index_imgPlot = list(range(0, num_epochs, int(num_epochs / 3))) + [num_epochs - 1]  # plot only 5 of all epochs
if num_epochs <= 5:
    FigRow = num_epochs
    index_imgPlot = range(num_epochs)  # plot all epochs

for k in index_imgPlot:
    plt.gray()
    imgs_train = outputs_train[k][1].detach().numpy()
    recon_train = outputs_train[k][2].detach().numpy()
    for i, item in enumerate(imgs_train):  # first row original images
        if i >= 9 or k > 0:
            break
        plt.subplot(FigRow + 1, 9, i + 1)
        item = item.reshape(-1, ImSize[0], ImSize[1])
        plt.axis('off')
        plt.imshow(item[0])

    for i, item in enumerate(recon_train):  # from second row on, generated images during training
        if i >= 9:
            break
        plt.subplot(FigRow + 1, 9, 9 + FigNum + 1)
        FigNum += 1
        item = item.reshape(-1, ImSize[0], ImSize[1])
        plt.axis('off')
        plt.imshow(item[0])

    plt.show()
fig1.suptitle('Evolution while Training ')

fig2 = plt.figure()
line_train, = plt.plot(LossHistory_train, label='Training')
line_test, = plt.plot(LossHistory_test, label='Testing')
plt.legend(handles=[line_train, line_test])
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Loss History')

fig3 = plt.figure()
for i in range(Layer2Notes):
    plt.subplot(5, 7, i + 1)
    plt.axis('off')
    plt.imshow(WeightLastLayer[num_epochs - 1][:, :, i].detach().numpy())
fig3.suptitle('Weight of Last Layer')


img_test = outputs_test[num_epochs - 1][1].detach().numpy()
recon_test = outputs_test[num_epochs - 1][2].detach().numpy()

fig4 = plt.figure()
for i, item in enumerate(img_test[100:-1]):  # first row original images
    if i >= 9:
        break
    plt.subplot(2, 9, i + 1)
    item = item.reshape(-1, ImSize[0], ImSize[1])
    plt.axis('off')
    plt.imshow(item[0])

for i, item in enumerate(recon_test[100:-1]):  # second row generated images
    if i >= 9:
        break
    plt.subplot(2, 9, 9 + i + 1)
    item = item.reshape(-1, ImSize[0], ImSize[1])
    plt.axis('off')
    plt.imshow(item[0])
fig4.suptitle('Testing Result')
